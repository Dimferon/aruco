#------------------------------------------------------
# PAckage information out
#------------------------------------------------------

if(BUILD_PACKAGE)
    message(STATUS)
    message(STATUS "--------------------------Package dir settings----------------------------")

    SET(PROJECT_DLLVERSION "${PROJECT_VERSION_MAJOR}${PROJECT_VERSION_MINOR}${PROJECT_VERSION_PATCH}")
    SET(PROJECT_PACKAGE_VERSION "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}")

    set(PROJECT_PACKAGE_DIR "${PROJECT_BINARY_DIR}/aruco_packages/aruco_${PROJECT_PACKAGE_VERSION}" CACHE PATH "Directory for package")

    CONFIGURE_FILE("${PROJECT_SOURCE_DIR}/cmake/config.cmake.in"
        "${PROJECT_PACKAGE_DIR}/${PROJECT_NAME}Config.cmake" @ONLY)
    INSTALL(FILES "${PROJECT_PACKAGE_DIR}/${PROJECT_NAME}Config.cmake" DESTINATION share/${PROJECT_NAME})

    CONFIGURE_FILE("${PROJECT_SOURCE_DIR}/cmake/configVersion.cmake.in"
        "${PROJECT_PACKAGE_DIR}/${PROJECT_NAME}ConfigVersion.cmake" @ONLY)
    INSTALL(FILES "${PROJECT_PACKAGE_DIR}/${PROJECT_NAME}ConfigVersion.cmake" DESTINATION share/${PROJECT_NAME})

    CONFIGURE_FILE("${PROJECT_SOURCE_DIR}/cmake/modules.cmake.in"
        "${PROJECT_PACKAGE_DIR}/bin/${PROJECT_NAME}Modules.cmake" @ONLY)
    INSTALL(FILES "${PROJECT_PACKAGE_DIR}/${PROJECT_NAME}Modules.cmake" DESTINATION share/${PROJECT_NAME})


    # теперь для каждого заданного пути
    set(toIncludePath "/include/${PROJECT_NAME}")
    message(STATUS "Copy dir -> ${PROJECT_PACKAGE_DIR}")
    message(STATUS "copy header to ${toIncludePath}")
    foreach(header ${headers})
        file(COPY "${PROJECT_SOURCE_DIR}/src/${header}" DESTINATION ${PROJECT_PACKAGE_DIR}/${toIncludePath})
    endforeach()

    message(STATUS "Copy fractal headers to ${toIncludePath}/fractallabelers")
    foreach(header ${fractal_headers})
        file(COPY "${PROJECT_SOURCE_DIR}/src/${header}" DESTINATION ${PROJECT_PACKAGE_DIR}/${toIncludePath}/fractallabelers)
    endforeach()

    message(STATUS "Copy dcf headers to ${toIncludePath}/dcf")
    foreach(header ${dcf_headers})
        file(COPY "${PROJECT_SOURCE_DIR}/src/${header}" DESTINATION ${PROJECT_PACKAGE_DIR}/${toIncludePath}/dcf)
    endforeach()

    add_custom_command(
        TARGET ${LIBNAME}
        POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${PROJECT_BINARY_DIR}/bin ${PROJECT_PACKAGE_DIR}/bin
        )

    message(STATUS)
    message(STATUS "--------------------------------------------------------------------------")
    message(STATUS)
endif()
