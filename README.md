# ArUco
License GPL3

Форк проекта https://sourceforge.net/projects/aruco/ с версии 3.1.12 ( оригинал в https://sourceforge.net/projects/aruco/)

## Задачи репозитория
1. Добавление генерации пакета библиотеки Aruco для подключение через cmake
1. Добавление GUI для существующих утилит Aruco